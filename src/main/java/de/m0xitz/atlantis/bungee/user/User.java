package de.m0xitz.atlantis.bungee.user;

import de.m0xitz.atlantis.bungee.Atlantis;
import de.m0xitz.atlantis.bungee.misc.LanguageType;
import lombok.Getter;
import lombok.Setter;
import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.util.UUID;

@Getter
@Setter
public class User {

    private final Atlantis atlantis;
    private final ProxiedPlayer proxiedPlayer;

    private String displayName;

    private LanguageType languageType;
    private ServerInfo serverInfo;


    public User( ProxiedPlayer proxiedPlayer ) {
        this.atlantis = Atlantis.getInstance( );
        this.proxiedPlayer = proxiedPlayer;

        this.displayName = proxiedPlayer.getName( );

        this.languageType = LanguageType.GERMAN;
    }

    public String getName( ) {
        return this.proxiedPlayer.getName( );
    }

    public UUID getUuid( ) {
        return this.proxiedPlayer.getUniqueId( );
    }

    public boolean hasPermission( String permission ) {
        return this.proxiedPlayer.hasPermission( permission );
    }

    public void sendMessage( ChatMessageType chatMessageType, String key, Object... objects ) {
        String message = this.translate( key, objects );
        switch ( chatMessageType ) {
            case SYSTEM:
                message = " §b§lAtlantis §8§l┃ §7" + message;
                break;
            case ACTION_BAR:
                this.proxiedPlayer.sendMessage( chatMessageType, new TextComponent( message ) );
                return;
        }
        this.proxiedPlayer.sendMessage( message );
    }

    public void sendMessage( String key, Object... objects ) {
        this.sendMessage( ChatMessageType.SYSTEM, key, objects );
    }

    public String translate( String key, Object... objects ) {
        String message = this.atlantis.getLanguageManager( ).getString( this.languageType, key );
        try {
            return String.format( message, objects );
        } catch ( Exception ex ) {
            return message;
        }
    }

    public int getPing( ) {
        return this.proxiedPlayer.getPing( );
    }

    public void setHeaderFooter( String header, String footer ) {
        this.proxiedPlayer.setTabHeader( new TextComponent( header ), new TextComponent( footer ) );
    }

    public void update( ) {
        String header = this.translate( "tablist.header" )
                .replace( "%server%", this.serverInfo.getName( ) )
                .replace( "%online%", String.valueOf( this.atlantis.getUserManager( ).getOnlineSize( ) ) )
                .replace( "%maxonline%", String.valueOf( 128 ) );
        String footer = this.translate( "tablist.footer" )
                .replace( "%server%", this.serverInfo.getName( ) )
                .replace( "%online%", String.valueOf( this.atlantis.getUserManager( ).getOnlineSize( ) ) )
                .replace( "%maxonline%", String.valueOf( 128 ) );

        this.setHeaderFooter( header, footer );
    }

    public void syncSettings( ) {

    }
}
