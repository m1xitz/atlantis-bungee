package de.m0xitz.atlantis.bungee.command;

import de.m0xitz.atlantis.bungee.user.User;

public class PingCommand extends AtlantisCommand {

    public PingCommand( ) {
        super( "ping" );
    }

    @Override
    public void onUser( User user, String[] strings ) {

        if ( strings.length == 1 && user.hasPermission( "atlantis.command.ping.other" ) ) {
            try {
                User target = this.getAtlantis( ).getUserManager( ).getUser( strings[ 0 ] );
                user.sendMessage( "message.command-ping.other", target.getDisplayName( ), target.getPing( ) );
                return;
            } catch ( NullPointerException ex ) {
                user.sendMessage( "message.user.not-online", strings[ 0 ] );
            }
            return;
        }

        user.sendMessage( "message.command-ping", user.getPing( ) );
    }
}
