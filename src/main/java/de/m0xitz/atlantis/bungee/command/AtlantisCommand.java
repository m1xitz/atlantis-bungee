package de.m0xitz.atlantis.bungee.command;

import de.m0xitz.atlantis.bungee.Atlantis;
import de.m0xitz.atlantis.bungee.user.User;
import lombok.Getter;
import lombok.Setter;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

@Getter
@Setter
public abstract class AtlantisCommand extends Command {

    private final Atlantis atlantis;
    private boolean console;

    private String description;
    private String usageMessage;
    private String commandPermission;

    public AtlantisCommand( String name, String... aliases ) {
        super( name, null, aliases );

        this.atlantis = Atlantis.getInstance( );
        this.console = false;

        this.commandPermission = "atlantis.command." + name;
    }

    @Override
    public void execute( CommandSender commandSender, String[] strings ) {
        if ( !( commandSender instanceof ProxiedPlayer ) ) {
            if ( this.console ) {
                this.onConsole( commandSender, strings );
                return;
            }
            commandSender.sendMessage( "§cDieser Command ist nur für Spieler gedacht§8." );
            return;
        }
        User user = this.atlantis.getUserManager( ).getUser( ( ProxiedPlayer ) commandSender );
        if ( !testPermission( user ) )
            return;
        this.onUser( user, strings );
        return;
    }

    public abstract void onUser( User user, String[] strings );

    public void onConsole( CommandSender commandSender, String[] strings ) {
    }

    public boolean testPermission( User user ) {
        if ( !( this.commandPermission == null || this.commandPermission.isEmpty( ) || user.getProxiedPlayer( ).hasPermission( this.commandPermission ) ) ) {
            user.sendMessage( "message.no-permission" );
            return false;
        }
        return true;
    }
}
