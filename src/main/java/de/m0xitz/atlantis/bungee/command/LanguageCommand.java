package de.m0xitz.atlantis.bungee.command;

import de.m0xitz.atlantis.bungee.misc.LanguageType;
import de.m0xitz.atlantis.bungee.user.User;

public class LanguageCommand extends AtlantisCommand {

    public LanguageCommand( ) {
        super( "language", "lang" );
    }

    @Override
    public void onUser( User user, String[] strings ) {
        if ( strings.length == 1 ) {
            try {
                String language = strings[ 0 ];

                LanguageType languageType = LanguageType.valueOf( language.toUpperCase( ) );
                user.setLanguageType( languageType );
                user.update( );

                user.sendMessage( "message.command-language", languageType.toString( ) );
                return;
            } catch ( IllegalArgumentException ignored ) {
            }
        }
        user.sendMessage( "message.command-language.help" );
    }
}
