package de.m0xitz.atlantis.bungee.manager;

import com.google.common.base.Charsets;
import com.google.common.collect.Maps;
import de.m0xitz.atlantis.bungee.Atlantis;
import de.m0xitz.atlantis.bungee.misc.LanguageType;
import lombok.Getter;

import java.io.*;
import java.util.Map;
import java.util.Properties;

@Getter
public class LanguageManager {

    private final Atlantis atlantis;

    private final Map<String, String> gerLanguage;
    private final Map<String, String> engLanguage;

    public LanguageManager( Atlantis atlantis ) {
        this.atlantis = atlantis;

        this.gerLanguage = Maps.newConcurrentMap( );
        this.engLanguage = Maps.newConcurrentMap( );

        this.loadMessages( );
    }

    public void loadMessages( ) {
        this.gerLanguage.clear( );
        this.engLanguage.clear( );

        Map<String, String> defaults = Maps.newHashMap( );
        defaults.put( "message.no-permission", "Message not set." );
        defaults.put( "message.command-ping", "Message not set" );
        defaults.put( "message.user-not-online", "Message not set" );
        defaults.put( "message.user-not-found", "Message not set" );
        defaults.put( "tablist.header", "Header not set" );
        defaults.put( "tablist.footer", "Footer not set" );

        this.load( defaults, this.gerLanguage, "german" );
        this.load( defaults, this.engLanguage, "english" );
    }

    public String getString( LanguageType languageType, String key ) {
        String message = "";

        switch ( languageType ) {
            case GERMAN:
                message = this.gerLanguage.getOrDefault( key, "§cMessage §8(§e" + key + "§8) §cnot found§8." );
                break;
            case ENGLISH:
                message = this.engLanguage.getOrDefault( key, "§cMessage §8(§e" + key + "§8) §cnot found§8." );
                break;
        }
        return message.replace( "&", "§" );
    }

    private void load( Map<String, String> defaultValues, Map<String, String> languageMap, String fileName ) {
        Properties properties = new Properties( );
        FileInputStream fileInputStream = null;
        InputStreamReader inputStreamReader = null;
        try {
            File folder = new File( "languages/Atlantis" );
            if ( !folder.exists( ) )
                folder.mkdirs( );

            File file = new File( folder.getPath( ), fileName + ".properties" );
            if ( !file.exists( ) ) {
                file.createNewFile( );
                this.loadDefaults( defaultValues, properties, file );
            } else {
                fileInputStream = new FileInputStream( file );
                inputStreamReader = new InputStreamReader( fileInputStream, Charsets.UTF_8 );

                properties.load( inputStreamReader );
            }
        } catch ( IOException ex ) {
            ex.printStackTrace( );
        } finally {
            try {
                assert fileInputStream != null;
                fileInputStream.close( );

                assert inputStreamReader != null;
                inputStreamReader.close( );
            } catch ( IOException ex ) {
                ex.printStackTrace( );
            } catch ( NullPointerException ignored ) {
            }
        }

        for ( Object key : properties.keySet( ) ) {
            languageMap.put( ( String ) key, properties.getProperty( ( String ) key ) );
        }
    }

    private void loadDefaults( Map<String, String> defaultValues, Properties properties, File file ) {
        FileOutputStream fileOutputStream = null;
        try {
            fileOutputStream = new FileOutputStream( file );
            properties.putAll( defaultValues );
            properties.save( fileOutputStream, "" );
        } catch ( IOException ex ) {
            ex.printStackTrace( );
        } finally {
            try {
                fileOutputStream.close( );
            } catch ( IOException ex ) {
                ex.printStackTrace( );
            }
        }
    }
}
