package de.m0xitz.atlantis.bungee.manager;

import com.google.common.collect.Lists;
import de.m0xitz.atlantis.bungee.Atlantis;
import de.m0xitz.atlantis.bungee.command.AtlantisCommand;
import lombok.Getter;

import java.util.List;

@Getter
public class CommandManager {

    private final Atlantis atlantis;
    private final List<AtlantisCommand> commands;

    public CommandManager( Atlantis atlantis ) {
        this.atlantis = atlantis;
        this.commands = Lists.newArrayList( );
    }

    public void register( AtlantisCommand atlantisCommand ) {
        this.atlantis.getProxy( ).getPluginManager( ).registerCommand( this.atlantis, atlantisCommand );
        this.commands.add( atlantisCommand );
    }
}
