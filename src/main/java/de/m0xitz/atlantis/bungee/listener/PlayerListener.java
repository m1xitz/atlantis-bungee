package de.m0xitz.atlantis.bungee.listener;

import de.m0xitz.atlantis.bungee.Atlantis;
import de.m0xitz.atlantis.bungee.user.User;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.PlayerDisconnectEvent;
import net.md_5.bungee.api.event.PostLoginEvent;
import net.md_5.bungee.api.event.ServerConnectedEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

public class PlayerListener implements Listener {

    private final Atlantis atlantis;

    public PlayerListener( Atlantis atlantis ) {
        this.atlantis = atlantis;
    }

    @EventHandler
    public void handleJoin( PostLoginEvent event ) {
        ProxiedPlayer proxiedPlayer = event.getPlayer( );
        User user = this.atlantis.getUserManager( ).registerUser( proxiedPlayer );

        this.setHeaderFooter( );
    }

    @EventHandler
    public void handleServerConnected( ServerConnectedEvent event ) {
        ProxiedPlayer proxiedPlayer = event.getPlayer( );
        User user = this.atlantis.getUserManager( ).getUser( proxiedPlayer );

        user.setServerInfo( event.getServer( ).getInfo( ) );

        this.setHeaderFooter( );
    }

    @EventHandler
    public void handleQuit( PlayerDisconnectEvent event ) {
        ProxiedPlayer proxiedPlayer = event.getPlayer( );
        User user = this.atlantis.getUserManager( ).unregisterUser( proxiedPlayer );

        this.setHeaderFooter( );
    }

    private void setHeaderFooter( ) {
        for ( User user : this.atlantis.getUserManager( ).getUsers( ) ) {
            user.update( );
        }
    }
}
