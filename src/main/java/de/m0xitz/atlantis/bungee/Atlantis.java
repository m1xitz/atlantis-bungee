package de.m0xitz.atlantis.bungee;

import de.m0xitz.atlantis.bungee.command.LanguageCommand;
import de.m0xitz.atlantis.bungee.command.PingCommand;
import de.m0xitz.atlantis.bungee.listener.PlayerListener;
import de.m0xitz.atlantis.bungee.manager.CommandManager;
import de.m0xitz.atlantis.bungee.manager.LanguageManager;
import de.m0xitz.atlantis.bungee.manager.UserManager;
import de.m0xitz.atlantis.bungee.misc.UuidFetcher;
import lombok.Getter;
import net.md_5.bungee.api.plugin.Plugin;

@Getter
public class Atlantis extends Plugin {

    @Getter
    private static Atlantis instance;

    private UuidFetcher uuidFetcher;

    private CommandManager commandManager;
    private LanguageManager languageManager;
    private UserManager userManager;

    @Override
    public void onEnable( ) {
        Atlantis.instance = this;

        this.uuidFetcher = new UuidFetcher( );

        this.commandManager = new CommandManager( this );
        this.languageManager = new LanguageManager( this );
        this.userManager = new UserManager( this );

        this.registerCommands( );
        this.registerListeners( );
    }

    private void registerCommands( ) {
        this.commandManager.register( new LanguageCommand( ) );
        this.commandManager.register( new PingCommand( ) );
    }

    private void registerListeners( ) {
        this.getProxy( ).getPluginManager( ).registerListener( this, new PlayerListener( this ) );
    }
}
